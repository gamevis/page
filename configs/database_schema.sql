--
-- PostgreSQL database dump
--

-- Dumped from database version 13.6 (Ubuntu 13.6-1.pgdg20.04+1+b1)
-- Dumped by pg_dump version 14.2

-- Started on 2022-05-16 15:18:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: beaazfixfcdwjb
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO beaazfixfcdwjb;

--
-- TOC entry 4034 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: beaazfixfcdwjb
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 201 (class 1259 OID 15542046)
-- Name: category; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public.category (
    id integer NOT NULL,
    name text NOT NULL,
    priority integer DEFAULT 1 NOT NULL,
    description text
);


ALTER TABLE public.category OWNER TO beaazfixfcdwjb;

--
-- TOC entry 200 (class 1259 OID 15542044)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO beaazfixfcdwjb;

--
-- TOC entry 4037 (class 0 OID 0)
-- Dependencies: 200
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: beaazfixfcdwjb
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- TOC entry 204 (class 1259 OID 15542431)
-- Name: role; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public.role (
    name text NOT NULL
);


ALTER TABLE public.role OWNER TO beaazfixfcdwjb;

--
-- TOC entry 203 (class 1259 OID 15542259)
-- Name: tag; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public.tag (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    category_id integer NOT NULL
);


ALTER TABLE public.tag OWNER TO beaazfixfcdwjb;

--
-- TOC entry 202 (class 1259 OID 15542257)
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE SEQUENCE public.tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO beaazfixfcdwjb;

--
-- TOC entry 4038 (class 0 OID 0)
-- Dependencies: 202
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: beaazfixfcdwjb
--

ALTER SEQUENCE public.tag_id_seq OWNED BY public.tag.id;


--
-- TOC entry 208 (class 1259 OID 16411966)
-- Name: tag_visualization; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public.tag_visualization (
    tag_id integer NOT NULL,
    visualization_id uuid NOT NULL,
    voter_id text NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.tag_visualization OWNER TO beaazfixfcdwjb;

--
-- TOC entry 207 (class 1259 OID 16411964)
-- Name: tag_visualization_id_seq; Type: SEQUENCE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE SEQUENCE public.tag_visualization_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_visualization_id_seq OWNER TO beaazfixfcdwjb;

--
-- TOC entry 4039 (class 0 OID 0)
-- Dependencies: 207
-- Name: tag_visualization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: beaazfixfcdwjb
--

ALTER SEQUENCE public.tag_visualization_id_seq OWNED BY public.tag_visualization.id;


--
-- TOC entry 205 (class 1259 OID 15739635)
-- Name: user; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public."user" (
    auth0_id text NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    role text DEFAULT 'user'::text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    last_seen timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."user" OWNER TO beaazfixfcdwjb;

--
-- TOC entry 206 (class 1259 OID 16411912)
-- Name: visualization; Type: TABLE; Schema: public; Owner: beaazfixfcdwjb
--

CREATE TABLE public.visualization (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    photo text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    release_year integer NOT NULL,
    note text NOT NULL,
    source text NOT NULL,
    canva_x integer DEFAULT 0 NOT NULL,
    canva_y integer DEFAULT 0 NOT NULL,
    canva_width integer DEFAULT 0 NOT NULL,
    canva_height integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.visualization OWNER TO beaazfixfcdwjb;

--
-- TOC entry 3867 (class 2604 OID 15542049)
-- Name: category id; Type: DEFAULT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- TOC entry 3869 (class 2604 OID 15542262)
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag ALTER COLUMN id SET DEFAULT nextval('public.tag_id_seq'::regclass);


--
-- TOC entry 3879 (class 2604 OID 16411969)
-- Name: tag_visualization id; Type: DEFAULT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization ALTER COLUMN id SET DEFAULT nextval('public.tag_visualization_id_seq'::regclass);


--
-- TOC entry 3881 (class 2606 OID 15542055)
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 3885 (class 2606 OID 15542438)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (name);


--
-- TOC entry 3883 (class 2606 OID 15542267)
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- TOC entry 3891 (class 2606 OID 16411974)
-- Name: tag_visualization tag_visualization_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization
    ADD CONSTRAINT tag_visualization_pkey PRIMARY KEY (id);


--
-- TOC entry 3893 (class 2606 OID 16422249)
-- Name: tag_visualization tag_visualization_tag_id_visualization_id_voter_id_key; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization
    ADD CONSTRAINT tag_visualization_tag_id_visualization_id_voter_id_key UNIQUE (tag_id, visualization_id, voter_id);


--
-- TOC entry 3887 (class 2606 OID 15739644)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (auth0_id);


--
-- TOC entry 3889 (class 2606 OID 16411920)
-- Name: visualization visualization_pkey; Type: CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.visualization
    ADD CONSTRAINT visualization_pkey PRIMARY KEY (id);


--
-- TOC entry 3894 (class 2606 OID 15542268)
-- Name: tag tag_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3896 (class 2606 OID 16411975)
-- Name: tag_visualization tag_visualization_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization
    ADD CONSTRAINT tag_visualization_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tag(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3898 (class 2606 OID 16411985)
-- Name: tag_visualization tag_visualization_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization
    ADD CONSTRAINT tag_visualization_user_id_fkey FOREIGN KEY (voter_id) REFERENCES public."user"(auth0_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3897 (class 2606 OID 16411980)
-- Name: tag_visualization tag_visualization_visualization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public.tag_visualization
    ADD CONSTRAINT tag_visualization_visualization_id_fkey FOREIGN KEY (visualization_id) REFERENCES public.visualization(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3895 (class 2606 OID 15739645)
-- Name: user user_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: beaazfixfcdwjb
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_role_fkey FOREIGN KEY (role) REFERENCES public.role(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 4035 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: beaazfixfcdwjb
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO beaazfixfcdwjb;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 4036 (class 0 OID 0)
-- Dependencies: 658
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO beaazfixfcdwjb;


-- Completed on 2022-05-16 15:19:06

--
-- PostgreSQL database dump complete
--

