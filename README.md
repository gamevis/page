# Game Vis Browser

## Description

Game Vis Browser is an implementation of the game visualizations browser created as part of the work on master thesis.

## Local debug

Documented in README in folder game-vis-browser.

## Project architecture set up and deployment

1. import /configs/database_schema.sql to PostgreSQL DB instance
2. connect Hasura to DB instance and import /configs/hasura_metadata.json to Hasura instance
3. created SPA Application in Auth0
4. add authentication rule to Auth Pipeline in Auth0 /configs/auth0/jwt-claim.js
5. add authentication rule to Auth Pipeline in Auth0 /configs/auth0/user-sync.js
6. provide Auth0 with access key for "x-hasura-admin-secret" used in authentication rule
7. set config strings in /game-vis-browser/src/utils/constants.ts
8. in /game-vis-browser run npm start for local use
9. use .gitlab-cy.yml pipeline for deployment to GitLab Pages

## Author

Bc. Márius Molčány
