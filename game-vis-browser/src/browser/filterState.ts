import { atom } from "recoil";
import { recoilPersist } from "recoil-persist";

const { persistAtom } = recoilPersist();

export const searchTextState = atom({
  key: "searchText",
  default: "",
  effects_UNSTABLE: [persistAtom],
});

export const searchTagsState = atom({
  key: "searchTags",
  default: [],
  effects_UNSTABLE: [persistAtom],
});

export const searchOrSwitch = atom({
  key: "searchOrSwitch",
  default: false,
  effects_UNSTABLE: [persistAtom],
});

export const orderState = atom({
  key: "orderState",
  default: 1,
  effects_UNSTABLE: [persistAtom],
});

export const orderAscState = atom({
  key: "orderAscState",
  default: -1,
  effects_UNSTABLE: [persistAtom],
});

export const sidebarState = atom({
  key: "sidebarState",
  default: false,
  effects_UNSTABLE: [persistAtom],
});

export const browserState = atom({
  key: "browserState",
  default: true,
  effects_UNSTABLE: [persistAtom],
});
