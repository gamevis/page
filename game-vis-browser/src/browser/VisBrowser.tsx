import { useQuery } from "@apollo/client";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cx from "classnames";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useRecoilState } from "recoil";
import {
  categoriesState,
  userIdState,
  visualizationsState,
} from "../auth/userState";
import VisCard from "../components/VisCard";
import VisEmpty from "../components/VisEmpty";
import VisError from "../components/VisError";
import VisLoading from "../components/VisLoading";
import Sidebar from "../home/Sidebar";
import { staticCategories } from "../utils/constants";
import {
  AllVisualization,
  AllVisualizationReply,
  Category,
} from "../utils/dtos";
import { GET_VISUALIZATIONS } from "../utils/queries";
import {
  browserState,
  orderAscState,
  orderState,
  searchOrSwitch,
  searchTagsState,
  searchTextState,
  sidebarState,
} from "./filterState";

function VisBrowser() {
  const { loading, error, data } =
    useQuery<AllVisualizationReply>(GET_VISUALIZATIONS);
  const [searchText, setSearchText] = useRecoilState(searchTextState);
  const [searchTags, setSearchTags] = useRecoilState(searchTagsState);
  const [sidebarShow, setSidebarShow] = useRecoilState(sidebarState);
  const [browserShow, setBrowserShow] = useRecoilState(browserState);
  const [vis, setVis] = useRecoilState<AllVisualization[]>(visualizationsState);
  const [categories, setCategories] =
    useRecoilState<Category[]>(categoriesState);
  const [filteredVis, setFilteredVis] = useState<AllVisualization[]>([]);
  const [orSwitch, setOrSwitch] = useRecoilState(searchOrSwitch);
  const [order, setOrder] = useRecoilState(orderState);
  const [orderAsc, setOrderAsc] = useRecoilState(orderAscState);
  const [appUserId, setAppUserIdState] = useRecoilState(userIdState);

  useEffect(() => {
    setSearchTags([]);
    setSearchText("");
    setOrSwitch(false);
    setOrder(1);
  }, []);

  useEffect(() => {
    if (data) {
      setVis(data.visualization);
      setCategories(data.category);
    }
  }, [data]);

  useEffect(() => {
    setFilteredVis(
      vis
        .filter((vis) => {
          const textFound = vis.name
            .toLocaleLowerCase()
            .includes(searchText.toLocaleLowerCase());
          const tagsFound = vis.tag_visualizations
            .filter((f) => searchTags.includes(f.tag_id as never))
            .every((tagVis) => {
              const category = data?.category.find(
                (f) => f.id === tagVis.tag.category_id
              );
              const categoryTags = category?.tags.length;
              const categoryVoteCount = category?.tags.reduce(
                (a, b) =>
                  a +
                  b.tag_visualizations.filter(
                    (f) => f.visualization_id === vis.id
                  ).length,
                0
              );
              const tagVoteCount = category?.tags.reduce(
                (a, b) =>
                  a +
                  b.tag_visualizations.filter(
                    (f) =>
                      f.visualization_id === vis.id &&
                      f.tag_id === tagVis.tag_id
                  ).length,
                0
              );

              const result =
                (tagVoteCount ? tagVoteCount : 0) >=
                Math.ceil(
                  (categoryVoteCount ? categoryVoteCount : 0) /
                    (categoryTags ? categoryTags : 1)
                );

              return result;
            });

          const emptyCheck = !(
            vis.tag_visualizations.filter((f) =>
              searchTags.includes(f.tag_id as never)
            ).length === 0
          );

          const discintTags = new Set(
            vis.tag_visualizations
              .filter((f) => searchTags.includes(f.tag_id as never))
              .map((x) => x.tag_id)
          );
          const andCheck = discintTags.size == searchTags.length;

          const emptySearch = !searchTags.length;

          return (
            textFound &&
            ((tagsFound && emptyCheck && (andCheck || orSwitch)) || emptySearch)
          );
        })
        .sort(ordering)
    );
  }, [searchText, searchTags, orSwitch, order, orderAsc, sidebarShow]);

  return (
    <>
      <Sidebar
        categories={categories}
        loading={loading && categories.length === 0}
      />

      <div
        className={cx("Browser", {
          fullBrowser: !sidebarShow,
        })}
      >
        {loading && filteredVis.length === 0 && <VisLoading />}
        {!loading && error && <VisError />}
        {!loading && !error && !filteredVis && <VisEmpty />}
        {!error && filteredVis && (
          <>
            {!sidebarShow && (
              <Button
                id="sidebar-show"
                className="mb-3"
                variant="outline-primary"
                onClick={() => {
                  setSidebarShow(true);
                  setBrowserShow(false);
                }}
              >
                <FontAwesomeIcon icon={faArrowRight} /> Show filters
              </Button>
            )}
            {browserShow &&
              filteredVis.map((vis) => {
                return (
                  <VisCard
                    key={"card" + vis.id}
                    categories={categories}
                    vis={vis}
                    user_id={appUserId}
                  />
                );
              })}
          </>
        )}
      </div>
    </>
  );

  function ordering(a: AllVisualization, b: AllVisualization) {
    switch (order) {
      case 1:
        return orderAsc * (Date.parse(b.created_at) - Date.parse(a.created_at));
      case 2:
        return (
          orderAsc *
          (a.tag_visualizations.reduce(
            (ai, bi) =>
              ai + (!staticCategories.includes(bi.tag.category_id) ? 1 : 0),
            0
          ) -
            b.tag_visualizations.reduce(
              (ai, bi) =>
                ai + (!staticCategories.includes(bi.tag.category_id) ? 1 : 0),
              0
            ))
        );
      case 3:
        if (a.release_year < b.release_year) {
          return -1 * orderAsc;
        }
        if (a.release_year > b.release_year) {
          return 1 * orderAsc;
        }
        return 0;
      default:
      case 0:
        return (
          orderAsc *
          a.name
            .trim()
            .localeCompare(b.name.trim(), "en", { sensitivity: "base" })
        );
    }
  }
}

export default VisBrowser;
