import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cx from "classnames";
import { ReactNode, useState } from "react";
import {
  Alert,
  Badge,
  Button,
  Col,
  Collapse,
  Modal,
  OverlayTrigger,
  Popover,
  Row,
  ToggleButtonGroup,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";
import { popupNoteState, returnUrlState } from "../auth/userState";
import CanvasImg from "../components/CanvasImg";
import { staticCategories } from "../utils/constants";
import { AllVisualization, Category } from "../utils/dtos";

type ItemPopUpProps = {
  vis: AllVisualization;
  categories: Category[];
  user_id: string;
  children: ReactNode;
};

function ItemPopUp(props: ItemPopUpProps) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [returnUrlValue, setReturnUrl] = useRecoilState(returnUrlState);
  const [popupNoteValue, setPopupNote] = useRecoilState(popupNoteState);
  const [voteInfo, setVoteInfo] = useState(false);

  return (
    <>
      <div className="viscard">
        <div onClick={handleShow}>{props.children}</div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header id="item-header" closeButton>
          <Modal.Title>
            <Row>
              <Col>
                {props.vis.name}
                <Badge
                  id="item-header-year"
                  className="year-badge"
                  bg="secondary"
                >
                  {props.vis.release_year}
                </Badge>
              </Col>
            </Row>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col className="Tags">
              {props.vis.tag_visualizations.map((x) => {
                if (staticCategories.includes(x.tag.category_id))
                  return (
                    <OverlayTrigger
                      key={x.tag.id}
                      placement="bottom"
                      overlay={
                        <Popover id={x.tag.id.toString()}>
                          <Popover.Header as="h3">
                            {x.tag.category.name}
                          </Popover.Header>
                          <Popover.Body>{x.tag.description}</Popover.Body>
                        </Popover>
                      }
                    >
                      <span className="badge">{x.tag.name}</span>
                    </OverlayTrigger>
                  );
              })}
            </Col>
          </Row>
          <Button
            onClick={() => {
              setPopupNote(!popupNoteValue);
            }}
            variant="light"
            aria-controls="vote-info"
            aria-expanded={popupNoteValue}
          >
            <h5 className="mb-0">
              <FontAwesomeIcon icon={faInfoCircle} />
            </h5>
          </Button>
          <Collapse in={popupNoteValue}>
            <div>
              <Alert
                id="vote-info"
                variant="info"
                onClose={() => {
                  setPopupNote(false);
                }}
                dismissible
                transition={false}
              >
                <Alert.Heading as="h5">Visualization taxonomy</Alert.Heading>
                <p>
                  Below are shown categories with tags that were voted by the
                  users. For more info visit{" "}
                  <Link to={"/about"} onClick={handleClose}>
                    About
                  </Link>{" "}
                  page.
                </p>
                <hr />
                <p className="mb-0">
                  To submit your votes press Vote in the bottom.
                </p>
              </Alert>
            </div>
          </Collapse>
          <Row>
            <div id="voting" className="categories">
              {props.vis.tag_visualizations.filter(
                (f) => !staticCategories.includes(f.tag.category_id)
              ).length == 0 && (
                <p>
                  <b>
                    No votes for this visualization, please continue by voting
                    here:
                  </b>
                  <Link to={"/vis/" + props.vis.id} onClick={handleClose}>
                    <Button variant="success" style={{ margin: "10px" }}>
                      Vote
                    </Button>
                  </Link>
                </p>
              )}
              {props.vis.tag_visualizations.filter(
                (f) => !staticCategories.includes(f.tag.category_id)
              ).length != 0 &&
                props.categories.map((category) => {
                  const categoryVotes = category.tags.reduce(
                    (a, b) =>
                      a +
                      b.tag_visualizations.filter(
                        (f) => f.visualization_id === props.vis.id
                      ).length,
                    0
                  );

                  const categoryTags = category.tags.length;
                  const voters = new Set(
                    props.vis.tag_visualizations
                      .filter((f) => category.id === f.tag.category_id)
                      .map((x) => x.voter_id)
                  ).size;
                  return (
                    !staticCategories.includes(category.id) &&
                    categoryVotes > 0 && (
                      <Row key={category.id}>
                        <Col>
                          <Link
                            className="link"
                            to={"/category/" + category.id}
                            onClick={() => setReturnUrl("/")}
                          >
                            <OverlayTrigger
                              placement="top"
                              overlay={
                                <Popover id={category.id.toString()}>
                                  <Popover.Header as="h3">
                                    Click for info
                                  </Popover.Header>
                                  <Popover.Body>
                                    {categoryVotes} vote
                                    {categoryVotes === 1 ? "" : "s"} by {voters}{" "}
                                    user{voters === 1 ? "" : "s"}
                                  </Popover.Body>
                                </Popover>
                              }
                            >
                              <strong>{category.name}</strong>
                            </OverlayTrigger>
                          </Link>
                          <ToggleButtonGroup type="checkbox">
                            {category.tags
                              .slice()
                              .sort((a, b) => {
                                return (
                                  b.tag_visualizations.filter(
                                    (f) => f.visualization_id === props.vis.id
                                  ).length -
                                  a.tag_visualizations.filter(
                                    (f) => f.visualization_id === props.vis.id
                                  ).length
                                );
                              })
                              .map((tag) => {
                                const tagVotes = tag.tag_visualizations.reduce(
                                  (a, b) =>
                                    a +
                                    (b.visualization_id === props.vis.id
                                      ? 1
                                      : 0),
                                  0
                                );

                                const highlighted =
                                  tagVotes >=
                                  Math.ceil(categoryVotes / categoryTags);

                                return (
                                  tagVotes > 0 &&
                                  highlighted && (
                                    <OverlayTrigger
                                      key={tag.id}
                                      placement="top"
                                      overlay={
                                        <Popover id={tag.id.toString()}>
                                          <Popover.Header as="h3">
                                            {tag.description}
                                          </Popover.Header>
                                          <Popover.Body>
                                            {tagVotes} vote
                                            {tagVotes == 1 ? "" : "s"}
                                          </Popover.Body>
                                        </Popover>
                                      }
                                    >
                                      <button
                                        type="button"
                                        id={tag.id.toString()}
                                        key={tag.id.toString()}
                                        className={cx("badge", {
                                          tagGrey: !highlighted,
                                          tagBadge: highlighted,
                                        })}
                                      >
                                        {tag.name}
                                      </button>
                                    </OverlayTrigger>
                                  )
                                );
                              })}
                          </ToggleButtonGroup>
                        </Col>
                      </Row>
                    )
                  );
                })}
              <div className="pt-3">
                <Alert id="vote-info" variant="secondary">
                  <h5>Statistics</h5>
                  <strong>Voted by:</strong>{" "}
                  {props.vis.tag_visualizations.filter(
                    (f) => !staticCategories.includes(f.tag.category_id)
                  ).length == 0
                    ? 0
                    : props.vis.tag_visualizations_aggregate.aggregate
                        .count}{" "}
                  users
                  <br />
                  <strong>Total votes:</strong>{" "}
                  {props.categories.reduce(
                    (a, b) =>
                      a +
                      (!staticCategories.includes(b.id)
                        ? b.tags.reduce(
                            (xa, xb) =>
                              xa +
                              xb.tag_visualizations.filter(
                                (x) => x.visualization_id === props.vis.id
                              ).length,
                            0
                          )
                        : 0),
                    0
                  )}
                </Alert>
              </div>
            </div>

            <Col>
              <a href={"#" + props.vis.id}>
                <CanvasImg
                  src={props.vis.photo}
                  pos_x={props.vis.canva_x}
                  pos_y={props.vis.canva_y}
                  width={props.vis.canva_width}
                  height={props.vis.canva_height}
                />
              </a>

              <a href="#" className="lightbox" id={props.vis.id}>
                <CanvasImg
                  src={props.vis.photo}
                  pos_x={props.vis.canva_x}
                  pos_y={props.vis.canva_y}
                  width={props.vis.canva_width}
                  height={props.vis.canva_height}
                />
              </a>
              <Row className="p-3">
                {(props.vis.note || props.vis.source) && (
                  <Alert id="vote-info" variant="secondary">
                    {props.vis.note && (
                      <p>
                        <b>Note:</b> {props.vis.note}
                      </p>
                    )}
                    {props.vis.note && props.vis.source && <hr />}
                    {props.vis.source && (
                      <p className="mb-0">
                        <b>Source: </b>
                        {props.vis.source}
                      </p>
                    )}
                  </Alert>
                )}
              </Row>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <a href={"./vis/" + props.vis.id}>
            <Button variant="success">Vote</Button>
          </a>
          <Button onClick={handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ItemPopUp;
