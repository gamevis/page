import { useQuery } from "@apollo/client";
import { useAuth0 } from "@auth0/auth0-react";
import { faArrowLeft, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import {
  Alert,
  Button,
  Col,
  Collapse,
  Container,
  OverlayTrigger,
  Placeholder,
  Popover,
  Row,
  Spinner,
  ToggleButtonGroup,
} from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useRecoilState } from "recoil";
import { returnUrlState, voteNoteState } from "../auth/userState";
import CanvasImg from "../components/CanvasImg";
import ErrorItemPage from "../components/ErrorItemPage";
import Vote from "../components/Vote";
import { staticCategories } from "../utils/constants";
import { VisualizationReply } from "../utils/dtos";
import { GET_VISUALIZATION } from "../utils/queries";

type ItemDetailProps = {
  id: string;
};

function ItemPage() {
  const navigate = useNavigate();
  const [voteNoteValue, setVoteNote] = useRecoilState(voteNoteState);
  const [returnUrlValue, setReturnUrl] = useRecoilState(returnUrlState);
  const [voteInfo, setVoteInfo] = useState(false);
  const { id } = useParams<ItemDetailProps>();
  const { isLoading, user, isAuthenticated } = useAuth0();
  const { loading, error, data } = useQuery<VisualizationReply>(
    GET_VISUALIZATION,
    {
      variables: { id: id, userId: user ? user.sub : "" },
    }
  );

  if (isLoading || loading) {
    return (
      <>
        <Container className="vis-page" fluid>
          <Button
            id="navigate-back"
            onClick={() => {
              navigate("/");
            }}
            variant="outline-dark"
          >
            <h1 className="mb-0">
              <FontAwesomeIcon icon={faArrowLeft} />{" "}
            </h1>
          </Button>
          <Row>
            <Col id="voting">
              <Placeholder as="h2" animation="wave">
                <Placeholder xs={4} size="lg" />{" "}
              </Placeholder>
              <Placeholder as="h5" animation="wave">
                <Placeholder xs={6} size="lg" />
              </Placeholder>
              <br />
              {[...Array(12)].map((x, index) => {
                return (
                  <Placeholder key={index} as="p" animation="wave">
                    <Placeholder xs={2} /> <Placeholder xs={3} />
                  </Placeholder>
                );
              })}

              <Spinner animation="border" role="status" aria-hidden="true" />
            </Col>
            <Col>
              <Placeholder as="div" animation="wave">
                <Placeholder xs={12} id="img-placeholder" />
              </Placeholder>
            </Col>
          </Row>
        </Container>
      </>
    );
  }

  if (error || !data) {
    return <ErrorItemPage />;
  }

  return (
    <>
      <Container className="vis-page" fluid>
        <Button
          id="navigate-back"
          onClick={() => {
            navigate("/");
          }}
          variant="outline-dark"
        >
          <h1 className="mb-0">
            <FontAwesomeIcon icon={faArrowLeft} />{" "}
          </h1>
        </Button>

        <Row>
          <Col id="voting" md="auto">
            <Row>
              <h2 className="">
                {data?.visualization_by_pk.name}{" "}
                <span className="year-badge badge">
                  {data?.visualization_by_pk.release_year}
                </span>
              </h2>
              <Col className="Tags">
                {data?.visualization_by_pk.tag_visualizations.map((vis) => {
                  if (staticCategories.includes(vis.tag.category_id))
                    return (
                      <span key={vis.tag.id} className="badge">
                        {vis.tag.name}
                      </span>
                    );
                })}
              </Col>
            </Row>
            <Row>
              <div className="categories">
                <Button
                  onClick={() => {
                    setVoteNote(!voteNoteValue);
                  }}
                  variant="light"
                  aria-controls="vote-info"
                  aria-expanded={voteNoteValue}
                >
                  <h5 className="mb-0">
                    <FontAwesomeIcon icon={faInfoCircle} />
                  </h5>
                </Button>
                <Collapse in={voteNoteValue}>
                  <div>
                    <Alert
                      id="vote-info"
                      variant="info"
                      onClose={() => {
                        setVoteNote(false);
                      }}
                      dismissible
                      transition={false}
                    >
                      <Alert.Heading>
                        Visualization taxonomy voting
                      </Alert.Heading>
                      <p>
                        Under this note you can see table with categories (e.g.
                        Task) and votable tags (e.g. Overview).
                      </p>
                      <hr />
                      <p className="mb-0">
                        Vote by clicking on button with tag that is relevant to
                        this visualization.
                      </p>
                    </Alert>
                  </div>
                </Collapse>
                <Alert
                  id="vote-info"
                  variant="warning"
                  show={!isAuthenticated && voteInfo}
                  onClose={() => {
                    setVoteInfo(false);
                  }}
                  dismissible
                  transition
                >
                  <Alert.Heading className="mb-0">
                    You must be logged in to vote
                  </Alert.Heading>
                </Alert>
                {data?.category.map((x) => {
                  const categoryVotes = x.tags.reduce(
                    (a, b) =>
                      a + b.tag_visualizations_aggregate.aggregate.count,
                    0
                  );

                  const voters = new Set(
                    data.visualization_by_pk.tag_visualizations
                      .filter((f) => x.id === f.tag.category_id)
                      .map((x) => x.voter_id)
                  ).size;

                  return (
                    !staticCategories.includes(x.id) && (
                      <Row key={x.id}>
                        <Col>
                          <Link
                            className="link"
                            to={"/category/" + x.id}
                            onClick={() => setReturnUrl("/vis/" + id)}
                          >
                            <OverlayTrigger
                              placement="top"
                              overlay={
                                <Popover id={x.id.toString()}>
                                  <Popover.Header as="h3">
                                    Click for info
                                  </Popover.Header>
                                  <Popover.Body>
                                    {categoryVotes} vote
                                    {categoryVotes === 1 ? "" : "s"} by {voters}{" "}
                                    user{voters === 1 ? "" : "s"}
                                  </Popover.Body>
                                </Popover>
                              }
                            >
                              <strong>{x.name}</strong>
                            </OverlayTrigger>
                          </Link>
                          <ToggleButtonGroup
                            type="checkbox"
                            onClick={() => {
                              setVoteInfo(!isAuthenticated);
                            }}
                          >
                            {x.tags
                              .slice()
                              .sort((a, b) => {
                                return (
                                  b.tag_visualizations_aggregate.aggregate
                                    .count -
                                  a.tag_visualizations_aggregate.aggregate.count
                                );
                              })
                              .map((tag) => {
                                return (
                                  <Vote
                                    key={tag.id}
                                    tag_id={tag.id}
                                    short_desc={tag.name}
                                    desc={tag.description}
                                    count={
                                      tag.tag_visualizations_aggregate.aggregate
                                        .count
                                    }
                                    total_votes={x.tags.reduce(
                                      (a, o) =>
                                        a +
                                        o.tag_visualizations_aggregate.aggregate
                                          .count,
                                      0
                                    )}
                                    tags_count={x.tags.length}
                                    voted={tag.tag_visualizations.length > 0}
                                    user_id={user?.sub ? user?.sub : ""}
                                    vis_id={data.visualization_by_pk.id}
                                  >
                                    {""}
                                  </Vote>
                                );
                              })}
                          </ToggleButtonGroup>
                        </Col>
                      </Row>
                    )
                  );
                })}
              </div>
            </Row>
            {(data?.visualization_by_pk.note ||
              data?.visualization_by_pk.source) && (
              <Alert id="vote-info" variant="secondary">
                {data?.visualization_by_pk.note && (
                  <p>
                    <b>Note:</b> {data?.visualization_by_pk.note}
                  </p>
                )}
                {data?.visualization_by_pk.note &&
                  data?.visualization_by_pk.source && <hr />}
                {data?.visualization_by_pk.source && (
                  <p className="mb-0">
                    <b>Source: </b>
                    {data?.visualization_by_pk.source}
                  </p>
                )}
                {(data?.visualization_by_pk.note ||
                  data?.visualization_by_pk.source) && <hr />}
                <h5>Statistics</h5>
                <strong>Voted by:</strong>{" "}
                {
                  data.visualization_by_pk.tag_visualizations_aggregate
                    .aggregate.count
                }{" "}
                users
                <br />
                <strong>Total votes:</strong>{" "}
                {data?.category.reduce(
                  (a, b) =>
                    a +
                    (!staticCategories.includes(b.id)
                      ? b.tags.reduce(
                          (xa, xb) =>
                            xa +
                            xb.tag_visualizations_aggregate.aggregate.count,
                          0
                        )
                      : 0),
                  0
                )}
              </Alert>
            )}
          </Col>

          <Col>
            <a href={"#" + data.visualization_by_pk.id}>
              <CanvasImg
                src={data.visualization_by_pk.photo}
                pos_x={data.visualization_by_pk.canva_x}
                pos_y={data.visualization_by_pk.canva_y}
                width={data.visualization_by_pk.canva_width}
                height={data.visualization_by_pk.canva_height}
              />
            </a>

            <a href="#" className="lightbox" id={data.visualization_by_pk.id}>
              <CanvasImg
                src={data.visualization_by_pk.photo}
                pos_x={data.visualization_by_pk.canva_x}
                pos_y={data.visualization_by_pk.canva_y}
                width={data.visualization_by_pk.canva_width}
                height={data.visualization_by_pk.canva_height}
              />
            </a>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default ItemPage;
