import { useEffect, useState } from "react";
import { MultiSelect } from "react-multi-select-component";
import { useRecoilState } from "recoil";
import { searchTagsState } from "../browser/filterState";
import { staticCategories } from "../utils/constants";
import { Category } from "../utils/dtos";
import { FilterOptions } from "../utils/utils";

type FilterSelectProps = {
  category: Category;
};

function FilterSelect(props: FilterSelectProps) {
  const [searchTags, setSearchTags] = useRecoilState(searchTagsState);
  const [selected, setSelected] = useState([]);
  const [helper, setHelper] = useState([]);

  const options: FilterOptions[] = props.category.tags.map((x) => {
    return { label: x.name, value: x.id };
  });

  function changeSearchTags() {
    const currentSearchTags = [...searchTags] as any[];
    const current = selected as unknown as FilterOptions[];
    const before = [...helper] as any[];
    let changes = [];

    if (before.length > current.length) {
      changes = before.filter((x) => !current.includes(x));
    } else if (before.length < current.length) {
      changes = current.filter((x) => !before.includes(x));
    } else {
      changes = [...current];
    }

    changes.map((x) => {
      if (before.length > current.length) {
        currentSearchTags.splice(currentSearchTags.indexOf(x.value), 1);
      } else {
        currentSearchTags.push(x.value);
      }
    });
    setHelper(selected);
    setSearchTags(currentSearchTags as []);
  }

  /*
  Persisent state of filter selection
  
  useEffect(() => {
    const currentSearchTags = [...searchTags] as any[];
    let selectedList = [] as any[];
    options.forEach((tag) => {
      if (currentSearchTags.includes(tag.value)) {
        selectedList.push(tag);
      }
    });
    setSelected(selectedList as []);
  }, []);
  */

  useEffect(() => {
    changeSearchTags();
  }, [selected]);

  return (
    <div className="mb-2">
      <b>{props.category.name}</b>{" "}
      <MultiSelect
        hasSelectAll={false}
        disableSearch={!staticCategories.includes(props.category.id)}
        options={options}
        value={selected}
        onChange={setSelected}
        labelledBy={props.category.name}
      />
    </div>
  );
}

export default FilterSelect;
