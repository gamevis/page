import { useState } from "react";
import { Popover } from "react-bootstrap";

type VoteCounterProps = {
  count: number;
  voted: boolean;
};

function VoteCounter(props: VoteCounterProps) {
  const [count, setCount] = useState(props.count);
  const [value, setValue] = useState(props.voted ? -1 : 1);

  function invert() {
    if (value != 0) setValue(0);
    else setValue(props.voted ? -1 : 1);
  }

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Diegetic components</Popover.Header>
      <Popover.Body>
        Based on two questions:
        <br />
        Is the interface component in the game story?
        <br />
        Is the component in the game space?
      </Popover.Body>
    </Popover>
  );

  return (
    <button
      onClick={() => {
        setCount(props.count + value);
        invert();
      }}
    >
      {count}
    </button>
  );
}

export default VoteCounter;
