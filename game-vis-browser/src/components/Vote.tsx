import { useMutation } from "@apollo/client";
import cx from "classnames";
import { ReactNode, useState } from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import { VOTE_ADD, VOTE_DEL } from "../utils/queries";

type VoteProps = {
  tag_id: number;
  desc: string;
  short_desc: string;
  count: number;
  total_votes: number;
  tags_count: number;
  voted: boolean;
  user_id: string;
  vis_id: string;
  children: ReactNode;
};

function Vote(props: VoteProps) {
  const [voted, setVoted] = useState(props.voted);
  const [value, setValue] = useState(props.voted ? 1 : 0);
  const [initValue, setInitValue] = useState(
    props.count - (props.voted ? 1 : 0)
  );
  const [voteAdd, { data: voteData, error: voteError }] = useMutation(VOTE_ADD);
  const [voteDel, { error: voteDelError }] = useMutation(VOTE_DEL);

  function vote() {
    if (!props.user_id) return;
    setValue(voted ? 0 : 1);
    if (voted) {
      voteDel({
        variables: {
          tag_id: props.tag_id,
          voter_id: props.user_id,
          visualization_id: props.vis_id,
        },
      });
    } else {
      voteAdd({
        variables: {
          tag_id: props.tag_id,
          voter_id: props.user_id,
          visualization_id: props.vis_id,
        },
      });
    }
    setVoted(!voted);
  }

  return (
    <OverlayTrigger
      placement="top"
      overlay={
        <Popover id={props.tag_id.toString()}>
          <Popover.Header as="h3">{props.desc}</Popover.Header>
          <Popover.Body>
            {initValue + value} vote
            {initValue + value === 1 ? "" : "s"}
          </Popover.Body>
        </Popover>
      }
    >
      <button
        type="button"
        id={props.tag_id.toString()}
        key={props.tag_id}
        className={cx("btn px-2", {
          vote: voted,
          novote: !voted,
        })}
        onClick={() => {
          vote();
        }}
      >
        {props.short_desc}
      </button>
    </OverlayTrigger>
  );
}

export default Vote;
