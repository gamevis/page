import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Alert } from "react-bootstrap";

function VisError() {
  return (
    <Alert variant="danger">
      <Alert.Heading>
        <FontAwesomeIcon
          style={{ color: "orange" }}
          icon={faTriangleExclamation}
        />{" "}
        Oh snap! You got an error!
      </Alert.Heading>
      <p>
        If you think this should be working please contact page administrator.
      </p>
    </Alert>
  );
}

export default VisError;
