import { Card, Col, ProgressBar, Row } from "react-bootstrap";
import { AllVisualization, Category } from "../utils/dtos";

type StatisticsProps = {
  vis: AllVisualization[];
  categories: Category[];
};

function StatisticsTable(props: StatisticsProps) {
  const colors = ["success", "dark", "danger", "warning", "secondary", ""];
  let i = 0;
  return (
    <div>
      <Card className="statistics">
        <Card.Body>
          <Row>
            <Col>Visualizations classified</Col>
            <Col>{props.vis.length}</Col>
          </Row>
          <hr />
          {props.categories.map((category) => {
            const categoryUses = category.tags.reduce(
              (a, b) => a + b.tag_visualizations.length,
              0
            );
            const colorVariant = colors[i++ % colors.length];
            return (
              <Row className="stats" key={category.id}>
                <Col>
                  <strong>{category.name}</strong>
                </Col>
                <Col>
                  <ProgressBar
                    variant={colorVariant}
                    max={categoryUses}
                    now={categoryUses}
                    label={`${categoryUses}`}
                  />
                </Col>
                <hr />
                {category.tags.map((tag) => (
                  <div key={tag.id} className="subCategory">
                    <Col>{tag.name}</Col>
                    <Col>
                      <ProgressBar
                        variant={colorVariant}
                        max={categoryUses}
                        now={tag.tag_visualizations_aggregate.aggregate.count}
                        label={`${tag.tag_visualizations_aggregate.aggregate.count}`}
                      />
                    </Col>
                  </div>
                ))}
              </Row>
            );
          })}
        </Card.Body>
      </Card>
    </div>
  );
}

export default StatisticsTable;
