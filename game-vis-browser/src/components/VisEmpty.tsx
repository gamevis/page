import { Card, Placeholder } from "react-bootstrap";

function VisEmpty() {
  return (
    <Card className="visualization">
      <Placeholder as="img" className="card-image img-fluid" />
      <Card.Body>
        <Card.Title>No visualizations found</Card.Title>
      </Card.Body>
    </Card>
  );
}

export default VisEmpty;
