import {
  faArrowLeft,
  faTriangleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Alert, Button, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

function ErrorItemPage() {
  const navigate = useNavigate();
  return (
    <Container className="vis-page" fluid>
      <Button
        id="navigate-back"
        onClick={() => {
          navigate("/");
        }}
        variant="outline-dark"
      >
        <h1 className="mb-0">
          <FontAwesomeIcon icon={faArrowLeft} />{" "}
        </h1>
      </Button>
      <Container>
        <Alert variant="danger">
          <Alert.Heading>
            <FontAwesomeIcon
              style={{ color: "orange" }}
              icon={faTriangleExclamation}
            />{" "}
            Oh snap! You got an error!
          </Alert.Heading>
          <p>
            If you think this should be working please contact page
            administrator.
          </p>
        </Alert>
      </Container>
    </Container>
  );
}

export default ErrorItemPage;
