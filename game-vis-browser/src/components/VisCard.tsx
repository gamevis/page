import { Card, Image } from "react-bootstrap";
import ItemPopUp from "../browser/ItemPopUp";
import { AllVisualization, Category } from "../utils/dtos";

type VisCardProps = {
  vis: AllVisualization;
  categories: Category[];
  user_id: string | undefined;
};

function VisCard(props: VisCardProps) {
  return (
    <ItemPopUp
      key={props.vis.id}
      vis={props.vis}
      categories={props.categories}
      user_id={props.user_id ? props.user_id : ""}
    >
      <Card className="visualization">
        <Image className="card-image" src={props.vis.photo} fluid />
        <Card.Body>
          <Card.Title>
            {props.vis.name.length > 19
              ? props.vis.name.substring(0, 18) + "..."
              : props.vis.name}
          </Card.Title>
        </Card.Body>
      </Card>
    </ItemPopUp>
  );
}

export default VisCard;
