import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Card, Placeholder, Spinner } from "react-bootstrap";
import { useRecoilState } from "recoil";
import { browserState, sidebarState } from "../browser/filterState";

function VisLoading() {
  const [sidebarShow, setSidebarShow] = useRecoilState(sidebarState);
  const [browserShow, setBrowserShow] = useRecoilState(browserState);

  return (
    <>
      {!sidebarShow && (
        <Button
          id="sidebar-show"
          className="mb-3"
          variant="outline-primary"
          onClick={() => {
            setSidebarShow(true);
            setBrowserShow(false);
          }}
        >
          <FontAwesomeIcon icon={faArrowRight} /> Show filters
        </Button>
      )}
      {[...Array(50)].map((x, index) => {
        return (
          <Card key={index} className="visualization">
            <Placeholder as="img" className="card-image img-fluid" />
            <Card.Body>
              <Card.Title>
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  style={{
                    margin: "0 0 0 0.5em",
                  }}
                />
              </Card.Title>
            </Card.Body>
          </Card>
        );
      })}
    </>
  );
}

export default VisLoading;
