import React, { useEffect, useRef } from "react";

type CanvasImgProps = {
  src: string;
  pos_x: number;
  pos_y: number;
  width: number;
  height: number;
};

function CanvasImg(props: CanvasImgProps) {
  let canvasRef = useRef<HTMLCanvasElement | null>(null);
  let canvasCtxRef = React.useRef<CanvasRenderingContext2D | null>(null);

  useEffect(() => {
    if (canvasRef.current) {
      canvasCtxRef.current = canvasRef.current.getContext("2d");
      let ctx = canvasCtxRef.current;
      const image = new Image();
      image.src = props.src;

      image.onload = function <HTMLImageElement>() {
        ctx!.canvas.width = this.width;
        ctx!.canvas.height = this.height;
        ctx!.drawImage(
          image,
          0,
          0,
          image.width,
          image.height,
          0,
          0,
          ctx!.canvas.width,
          ctx!.canvas.height
        );
        ctx!.strokeStyle = "#FF0000";
        ctx!.lineWidth = 7;
        ctx!.strokeRect(props.pos_x, props.pos_y, props.width, props.height);
      };
    }
  }, []);

  return (
    <div className="img-container">
      <canvas ref={canvasRef} className="img-fluid"></canvas>
    </div>
  );
}

export default CanvasImg;
