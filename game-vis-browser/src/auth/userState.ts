import { atom } from "recoil";
import { recoilPersist } from "recoil-persist";
import { AllVisualization, Category } from "./../utils/dtos";

const { persistAtom } = recoilPersist();

export const userIdState = atom({
  key: "userId",
  default: "",
  effects_UNSTABLE: [persistAtom],
});

export const userEmailState = atom({
  key: "userEmail",
  default: "",
  effects_UNSTABLE: [persistAtom],
});

export const userAuthState = atom({
  key: "userAuth",
  default: false,
  effects_UNSTABLE: [persistAtom],
});

export const userTokenState = atom({
  key: "userToken",
  default: "",
  effects_UNSTABLE: [persistAtom],
});

export const voteNoteState = atom({
  key: "voteNote",
  default: true,
  effects_UNSTABLE: [persistAtom],
});

export const popupNoteState = atom({
  key: "popupNote",
  default: true,
  effects_UNSTABLE: [persistAtom],
});

export const returnUrlState = atom({
  key: "returnUrl",
  default: "/",
  effects_UNSTABLE: [persistAtom],
});

export const visualizationsState = atom({
  key: "visualizationsState",
  default: [] as AllVisualization[],
  effects_UNSTABLE: [persistAtom],
});

export const categoriesState = atom({
  key: "categoriesState",
  default: [] as Category[],
  effects_UNSTABLE: [persistAtom],
});
