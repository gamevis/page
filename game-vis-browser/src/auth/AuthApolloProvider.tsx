import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache,
} from "@apollo/client";
import { setContext } from "@apollo/link-context";
import { useAuth0 } from "@auth0/auth0-react";
import { ReactNode, useEffect } from "react";
import { useRecoilState } from "recoil";
import { hasuraUrl } from "../utils/constants";
import {
  userAuthState,
  userEmailState,
  userIdState,
  userTokenState,
} from "./userState";

type AuthApolloProviderProps = {
  children: ReactNode;
};

function AuthApolloProvider(props: AuthApolloProviderProps) {
  const { getAccessTokenSilently } = useAuth0();
  const [appUserId, setAppUserIdState] = useRecoilState(userIdState);
  const [appUserEmail, setAppUserEmailState] = useRecoilState(userEmailState);
  const [appUserAuth, setAppUserAuthState] = useRecoilState(userAuthState);
  const [appUserToken, setAppUserTokenState] = useRecoilState(userTokenState);
  const { user, isAuthenticated } = useAuth0();

  useEffect(() => {
    setAppUserAuthState(isAuthenticated);
  }, [isAuthenticated]);

  useEffect(() => {
    if (user) {
      setAppUserIdState(user.sub);
      setAppUserEmailState(user.email);
    }
  }, [user]);

  useEffect(() => {
    const getToken = async () => {
      if (isAuthenticated) {
        const token = await getAccessTokenSilently();
        setAppUserTokenState(token);
        localStorage.setItem("token", token);
      } else {
        localStorage.setItem("token", "");
      }
    };
    getToken();
  }, [user]);

  const httpLink = createHttpLink({
    uri: hasuraUrl,
  });

  const authLink = setContext(async () => {
    const token = localStorage.getItem("token");
    return {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
  });

  const apolloClient = new ApolloClient({
    link: isAuthenticated ? authLink.concat(httpLink) : httpLink,
    cache: new InMemoryCache(),
    connectToDevTools: true,
  });

  return (
    <ApolloProvider client={apolloClient}>{props.children}</ApolloProvider>
  );
}

export default AuthApolloProvider;
