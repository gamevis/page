import { useAuth0 } from "@auth0/auth0-react";
import { useRecoilState } from "recoil";
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";
import { userAuthState } from "./userState";

function AuthButton() {
  const { isLoading } = useAuth0();
  const [appUserAuth, setAppUserAuthState] = useRecoilState(userAuthState);

  /*if (isLoading && !appUserAuth) {
    return (
      <Spinner
        as="span"
        animation="border"
        size="sm"
        role="status"
        aria-hidden="true"
      />
    );
  }*/

  return appUserAuth ? <LogoutButton /> : <LoginButton />;
}

export default AuthButton;
