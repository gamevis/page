import { useAuth0 } from "@auth0/auth0-react";

function LoginButton() {
  const { loginWithRedirect } = useAuth0();

  return (
    <a className="nav-link auth-button" onClick={() => loginWithRedirect()}>
      Log In
    </a>
  );
}

export default LoginButton;
