import { useAuth0 } from "@auth0/auth0-react";
import { Container, Image, Placeholder, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useRecoilState } from "recoil";
import {
  userAuthState,
  userEmailState,
  userIdState,
  userTokenState,
} from "./userState";

function Profile() {
  const { user, isLoading, isAuthenticated } = useAuth0();
  const [appUserId, setAppUserIdState] = useRecoilState(userIdState);
  const [appUserEmail, setAppUserEmailState] = useRecoilState(userEmailState);
  const [appUserAuth, setAppUserAuthState] = useRecoilState(userAuthState);
  const [appUserToken, setAppUserTokenState] = useRecoilState(userTokenState);
  const navigate = useNavigate();

  if (isLoading) {
    return (
      <Container className="about">
        <h2>My profile</h2>
        <p>
          <Placeholder as="p" animation="wave">
            <Placeholder xs={6} />
          </Placeholder>
          <Placeholder as="p" animation="wave">
            <Placeholder xs={6} />
          </Placeholder>
          <Placeholder as="p" animation="wave">
            <Placeholder xs={6} />
          </Placeholder>
        </p>
        <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
        />
      </Container>
    );
  }

  if (!isAuthenticated) {
    navigate("/");
  }

  return (
    <Container className="about">
      <h2>My profile</h2>
      <div>
        <Image src={user?.picture} fluid roundedCircle />
        <p>
          <br />
          <b>Email: </b> {appUserEmail}
        </p>
      </div>
    </Container>
  );
}

export default Profile;
