import { useApolloClient } from "@apollo/client";
import { useAuth0 } from "@auth0/auth0-react";
import { useResetRecoilState } from "recoil";
import { publicUrl } from "../utils/constants";
import { userIdState } from "./userState";

function LogoutButton() {
  const { logout } = useAuth0();
  const client = useApolloClient();
  const resetUserState = useResetRecoilState(userIdState);
  return (
    <a
      className="nav-link auth-button"
      onClick={() => {
        logout({
          returnTo:
            process.env.NODE_ENV == "development"
              ? window.location.origin
              : publicUrl,
        });
        resetUserState;
        client.resetStore();
      }}
    >
      Log Out
    </a>
  );
}

export default LogoutButton;
