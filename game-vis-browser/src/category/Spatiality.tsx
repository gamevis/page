import { Container, Image } from "react-bootstrap";

function Spatiality() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Spatiality</h2>
        <p>
          With the concept of diegesis, we examine how the immersion of
          visualizations affects the user interface in games. In chapter 5.1.5.
          we have explored the immersion of the visualizations in games, but our
          focus was on whether visualization is immersive or informative only
          and nothing more specific in terms of diegesis. Also, if visualization
          is integrated into the game or separated was categorized. When
          deciding whether the visualization is diegetic or not, we often focus
          on the single specific component of the game interface. To decide
          which group such visual representation belongs to, we have to answer
          two questions.
          <br />
          <br />
          <i>Is the representation visualized in the 3D game space?</i>
          <br />
          <i>Is the representation existing in the fictional game world?</i>
          <br />
          <br />
          <Image
            src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/spatiality.png"
            fluid
            rounded
            style={{ height: "300px" }}
          />
          <br />
          Based on the answers to these questions, we can categorize
          visualizations into these four groups:
        </p>
        <h4>Non-diegetic visualization</h4>
        <p>
          Representation of this type of visualization is HUD in most games. An
          example of this is typically the HUD. A game such as World of
          Warcraft, with its particularly complex user interface, is an example
          of this category. Visualization of the UI is not present in the 3D
          game space and is not a part of the game narrative.
        </p>
        <Image
          src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/hud.jpg"
          fluid
          rounded
          style={{ height: "200px" }}
        />
        <p>Non-diegetic visualization of HUD in World Of Warcraft</p>
        <h4>Spatial visualization</h4>
        <p>
          A visualization that is present in the 3D game space of the game and
          is not relevant to the game narrative is an example of spatial
          representation. In the strategy game Supreme Commander 2, the circular
          light blue aura around a selected unit indicates which unit is
          currently selected, as shown in image below.
        </p>
        <Image
          src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/supreme-spatiality.jpg"
          fluid
          rounded
          style={{ height: "200px" }}
        />
        <p>Spatial visualization of unit selection in Supreme Commander 2</p>
        <h4>Meta visualization</h4>
        <p>
          Meta visualization is represented as part of the narrative/story of
          the game but not represented within 3D space. Typically these
          representations refer to a status of a game character. Commonly used
          in shooter games are displays of blood splatter on the screen or
          toning colors to red.
        </p>
        <h4>Diegetic visualization</h4>
        <p>
          A visualization that is present in the 3D game world while being part
          of the game narrative is diegetic. Diegetic visualizations are
          recognized by the player character as well as the player. As an
          example, Game Dead Space visualizes the health bar as part of the
          high-tech suit of the character.
        </p>
        <Image
          src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/Dead-Space-02.jpg"
          fluid
          rounded
          style={{ height: "200px" }}
        />
        <p>Diegetic visualization of health bar in Dead Space</p>
      </Container>
    </Container>
  );
}

export default Spatiality;
