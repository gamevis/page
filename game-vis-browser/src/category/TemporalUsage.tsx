import { Container } from "react-bootstrap";

function TemportalUsage() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Temporal Usage</h2>
        <p>
          Depending on the type of feedback the game tries to achieve,
          visualization can be visible permanently or after some trigger. By
          differentiating the targeted temporal usage of such visualization, we
          recognize these types:
        </p>
        <h4>Continuous</h4>
        <p>
          Visualizations displayed continuously are the most common. They
          provide live feedback to the player about the current game state. A
          typical example is the health bar and other variations of bar
          visualizations are integrated into the heads-up display.
        </p>
        <h4>Intermittent</h4>
        <p>
          Intermittent visualizations are not displayed continuously to the
          player. They are triggered by either player input or as a response to
          the event of the game. Typical examples are maps, inventories opened
          by a player trigger, or overlays indicating the direction from which
          the player is being hit.
        </p>
        <h4>Retrospective</h4>
        <p>
          Typically these visualizations are used outside of the main gameplay
          loop. The primary purpose is to provide users with a retrospective of
          their performance. A representative case is replay theater or
          statistics displayed at the end of the match in strategy games such as
          Age of Empires II.
        </p>
        <h4>Prospective</h4>
        <p>
          Prospective visualizations intend to provide predictions about future
          outcomes. It is often used in role-playing games in dialogs to signal
          if the choice will be executed successfully or if it has some
          emotional undertone. In Fallout 4 players can utilize the V.A.T.S
          overlay, in which the name of the NPC (non-playable character) is seen
          simply in either green or red color overlay to indicate friend or foe
          status.
        </p>
      </Container>
    </Container>
  );
}

export default TemportalUsage;
