import { Container, ListGroup } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

function VisualizationTask() {
  const navigate = useNavigate();
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Visualization task</h2>
        <p>
          We have appropriated the classification of text visualization to
          categorize game visualizations. If such classification is not
          reasonable for specific visualization, it can be ignored.
          Visualization task categories:
        </p>
        <ListGroup>
          <ListGroup.Item>Overview</ListGroup.Item>
          <ListGroup.Item>Comparison</ListGroup.Item>
          <ListGroup.Item>Categorization</ListGroup.Item>
          <ListGroup.Item>Monitoring</ListGroup.Item>
          <ListGroup.Item>Navigation / Exploration</ListGroup.Item>
          <ListGroup.Item>
            Region of interest – e.g. hit indicator in shooter games, in
            strategy games a warning that something needs a player's attention
            in some specific place on the game map
          </ListGroup.Item>
        </ListGroup>
      </Container>
    </Container>
  );
}

export default VisualizationTask;
