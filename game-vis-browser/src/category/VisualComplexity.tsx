import { Container } from "react-bootstrap";

function VisualComplexity() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Visual Complexity</h2>
        <p>
          Classifying the visual complexity of visualization is not exclusive to
          game visualizations. Author of frameworks remarks that this metric can
          help explore if the newer games are getting more complex and if this
          correlates with the level of visual literacy expected from the players
          by the game developers.{" "}
        </p>
        <h4>Basic</h4>
        <p>
          Very simple visualizations like health bars, ammo counters, weapon
          slots, and others used since the inception of video games are typical
          examples of this category.{" "}
        </p>
        <h4>Intermediate</h4>
        <p>
          More advanced composition is used for intermediate complexity than the
          basic one. Examples: score statistics in multiplayer games, summary
          replay/animations, in-game navigational elements, ludophasmas, etc.
        </p>
        <h4>Advanced </h4>
        <p>
          These visualizations expect advanced visual literacy from the users of
          such representation. We can say that casual users do not see these
          visualizations daily. The line between intermediate and advanced
          representations is unclear, and the selection of one above the other
          can be debatable. Anything above the complexity of the before
          mentioned classes belongs to this class. Practical examples range from
          diplomacy graphs displaying relations of factions in the Civilization
          series of games or hierarchies/trees representing progression in a
          complex manner.
        </p>
      </Container>
    </Container>
  );
}

export default VisualComplexity;
