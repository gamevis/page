import { Container, ListGroup } from "react-bootstrap";

function Visualization() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Common visualization classification</h2>
        <p>
          Lastly, we will categorize visualization based on dimensionality,
          representation, or alignment techniques. The following lists consist
          of the standard categories found across other visualization taxonomies
          applicable for game visualizations.
        </p>
        <h4>Dimensionality</h4>
        <ListGroup horizontal>
          <ListGroup.Item>2D</ListGroup.Item>
          <ListGroup.Item>3D</ListGroup.Item>
        </ListGroup>
        <hr />
        <h4>Representation</h4>
        <ListGroup horizontal>
          <ListGroup.Item>Line plot / River</ListGroup.Item>
          <ListGroup.Item>Pixel / Area / Matrix</ListGroup.Item>
          <ListGroup.Item>Node-link</ListGroup.Item>
          <ListGroup.Item>Map</ListGroup.Item>
          <ListGroup.Item>Text</ListGroup.Item>
          <ListGroup.Item>Glyph / Icon</ListGroup.Item>
        </ListGroup>
        <hr />
        <h4>Alignment</h4>
        <ListGroup horizontal>
          <ListGroup.Item>Linear / Parallel</ListGroup.Item>
          <ListGroup.Item>Radial</ListGroup.Item>
          <ListGroup.Item>Metric-dependent</ListGroup.Item>
        </ListGroup>
      </Container>
    </Container>
  );
}

export default Visualization;
