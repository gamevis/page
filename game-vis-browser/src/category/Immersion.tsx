import { Container } from "react-bootstrap";

function Immersion() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Immersion</h2>
        <p>
          Spatial immersion in visualization is a vital aspect of this
          classification. Differentiating whether the visual representation
          belongs to the game narrative or if the visualization is represented
          within a 3D game space are the factors upon which this category is
          derived.
        </p>
        <h4>Immersive/Integrated</h4>
        <p>
          Visualizations fitting this category are present in the game universe
          itself. They do not break the game atmosphere in any way. The exact
          representation of visualization is dependent on the game genre. In
          economic simulations, graphs and plots are a natural part of the game.
          The hit direction indicator is also considered immersive/integrated in
          the case of shooter games. The information conveyed by it is essential
          for the player. This indicator is just a tool to provide information
          to the player while being limited by the immersion of the hardware
          setup.
        </p>
        <h4>Informative/Integrated</h4>
        <p>
          Unlike the immersive/integrated category, these visualizations provide
          information that the player would not have as part of the native game
          world. Such examples are performance charts, replay theaters,
          spectating cameras, or even an overlay of other game characters that
          are not in a clear line of sight, essentially functioning as so-called
          "wallhack," often used in shooter games to provide info about allies
          in cooperative mode or in replay theater.
        </p>
        <h4>Immersive/Separate</h4>
        <p>
          Immersive and separate visual representations can be found in examples
          such as the Destiny 2 Companion mobile app, where players can interact
          with other players, review progression, and manage their inventory.
          The main aspect is that this type of visualization is part of the game
          world, but it is not included in the game itself.
        </p>
        <h4>Informative/Separate</h4>
        <p>
          Visual representations that are outside of the game and provide
          informative content. Practical examples are open data APIs in games
          such as Counter-Strike: Global Offensive with players' statistics,
          maps, etc. Another instance of this type of visualization are fan-made
          maps with guidance information for Escape from Tarkov or even content
          from the game encyclopedias.
        </p>
      </Container>
    </Container>
  );
}

export default Immersion;
