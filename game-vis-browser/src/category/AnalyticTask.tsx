import { Container } from "react-bootstrap";

function AnalyticTask() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Analytic task</h2>
        <p>
          This section describes the analytic task that visualization fulfills
          with its representation. No previous research was found that delves
          into this topic in the sphere of game visualization.
        </p>
        <h4>Sentiment analysis</h4>
        <p>
          Visualization analyzes sentiment, opinion, and affection. We have
          often seen an example of this in games visualization that enemy
          characters are highlighted in red and friendly ones in green.
        </p>
        <h4>Event analysis</h4>
        <p>
          The task of analyzing events is often used in replay theater or
          summary display on the game session end.
        </p>
        <h4>Relation / Connection analysis</h4>
        <p>
          Representations that compare relationships between data items or try
          to visualize the connection between them belong to this class.
        </p>
        <h4>Summarization</h4>
        <p>
          The task of summarization is often recurring with video games as a way
          to provide feedback to the players to explain the interconnections of
          the gameplay mechanics.
        </p>
      </Container>
    </Container>
  );
}

export default AnalyticTask;
