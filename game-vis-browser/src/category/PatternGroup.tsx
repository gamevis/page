import { Container, Image, ListGroup } from "react-bootstrap";

function PatternGroup() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Pattern group</h2>
        <p>
          These patterns result from the aggregation of common uses of
          visualizations in games. By using these recognized patterns to
          classify visualizations of our browser, we can provide further proof
          that this taxonomy is working as expected and improve our
          application's filtering functionality. To include a group for any
          different visualization pattern, we also include the category "other"
          in this category type. These groups are not mutually exclusive. The
          resulting classification:
          <br />
          <br />
          <ListGroup horizontal>
            <ListGroup.Item>Heads-up display (HUD)</ListGroup.Item>
            <ListGroup.Item>Replay theater</ListGroup.Item>
            <ListGroup.Item>Progression</ListGroup.Item>
            <ListGroup.Item>Data in space</ListGroup.Item>
            <ListGroup.Item>Data in time</ListGroup.Item>
            <ListGroup.Item>Other</ListGroup.Item>
          </ListGroup>
        </p>
        <h4>Heads-up display (HUD)</h4>
        <p>
          A heads-up display (HUD) is a visual representation that presents
          information as part of the game user interface. Typically multiple
          separate values are displayed. The types of values shown on HUD depend
          on the game genre and theme. For example, shooter games commonly
          display health, time, ammo count, and more as part of the HUD. These
          visualizations are displayed as an overlay over the gameplay of the
          game.
        </p>
        <h4>Replay theater</h4>
        <p>
          Replay theater refers to a tool implemented inside the game that
          allows replay action that happened as part of the game session. This
          replay can be either partial, as used in the game series Call Of Duty,
          where "killCam" shows players' death from the killer's perspective. In
          strategy games such as StarCraft II, replays of the entire match are
          another example. Replay theaters are not simple video recordings of
          the game session, but they often include additional visualizations,
          further enhancing the experience of the observer. Such examples are
          time controls of the recorded session, space controls that allow to
          use of different viewpoints, and also event timeline information.{" "}
        </p>
        <Image
          src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/playback.png"
          fluid
          rounded
        />
        <p>
          Playback control interface in Call of Duty: Black Ops. The timeline
          visualizes events such as kills and death with different color
          stripes.
        </p>

        <h4>Progression tree</h4>
        <p>
          The category was initially named "progression tree," but from our
          observation of game visualizations, we have concluded that
          generalizing this category to "progression" would be a better
          characterization. Progression tress are directed graphs that visualize
          the player's progression in the game. This kind of visualization
          allows to present the player with available progression options while
          allowing them to identify dependencies that are needed to advance
          quickly. This kind of visualization is also used in the case of simple
          progression where there is little to no branching of options
          occurring. Recuring types of progression trees are skill and
          technology trees, with the main difference being the type of the
          advancing value.
        </p>
        <Image
          src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/progression-tree.png"
          fluid
          rounded
        />
        <p>
          Simple progression with two branches from the start point in Tom
          Clancy's The Division 2. Progression of the in-game item properties.
        </p>
        <h4>Data in space</h4>
        <p>
          The visualization that represents spatial game data belongs to pattern
          group data in space. Common examples are:
          <ListGroup>
            <ListGroup.Item>
              Heatmap – statistical value displayed spatialy
            </ListGroup.Item>
            <ListGroup.Item>
              Fog of war – unexplored or unobserved areas visualized differently
            </ListGroup.Item>
            <ListGroup.Item>Annot ated map</ListGroup.Item>
            <ListGroup.Item>
              Influence map – territory control displayed with visual aid
            </ListGroup.Item>
          </ListGroup>
        </p>
        <h4>Data in time</h4>
        <p>
          Visualizing time series data can help players when analyzing past
          gameplay. Such visualizations are often retrospective. An event
          timeline is an example of replay theater, as we previously stated.
          However, it is also an example of data in time. From such an example,
          we can infer that this is one of many examples that these groups are
          not mutually exclusive.
        </p>
      </Container>
    </Container>
  );
}

export default PatternGroup;
