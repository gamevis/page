import { Container } from "react-bootstrap";

function PrimaryPurpose() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Primary Purpose</h2>
        <p>
          Game developers use visualizations to convey some information to the
          player. This information has its purpose in terms of the game. The
          primary purpose of said visualizations can be hard to classify as we
          are only deducting the reasoning why such visualization was used; this
          can lead to inconsistencies. The primary purpose has the following
          classes:
        </p>
        <h4>Status</h4>
        <p>
          The intended use of status visualizations is to communicate important
          information to the player. Typical examples are health bars, ammo
          counters, and indicators of hit direction. These visualizations are
          typically continuously displayed or triggered by some action. Simple
          numerical values are often replaced by visual representations of these
          values, such as health bars instead of the percentual value of health.
        </p>
        <h4>Training</h4>
        <p>
          Such visualizations aim to give feedback to the players, yourself, or
          others to improve themselves and understand the gameplay better.
          Examples of this are replays theaters or continual ones such as visual
          overlays in vehicle simulations displaying the optimal path on track.
        </p>
        <h4>Progression</h4>
        <p>
          In multiplayer or social games, communication is essential for
          gameplay. This can be between players actually playing the game or
          even spectators. Another common type of communication is spectating
          professional players in official e-sport tournaments with commentators
          or other viewers. Sharing game progress on social media platforms with
          some visualization of the improvement of stats is an example of this
          purpose of visualization.
        </p>
        <h4>Communication</h4>
        <p>
          In multiplayer or social games, communication is essential for
          gameplay. This can be between players actually playing the game or
          even spectators. Another common type of communication is spectating
          professional players in official e-sport tournaments with commentators
          or other viewers. Sharing game progress on social media platforms with
          some visualization of the improvement of stats is an example of this
          purpose of visualization.
        </p>
        <h4>Debugging and balancing</h4>
        <p>
          Other categories are mostly focused on the players. However, there is
          also a strong incentive for visualizations targeted at game
          developers. There are many metrics developers are interested in to
          balance or improve in their games. Visualizations such as heat maps of
          players' activity on the map are used. Often special tools are
          created, such as BioWare's telemetry system called SkyNet or others.
        </p>
      </Container>
    </Container>
  );
}

export default PrimaryPurpose;
