import { Container } from "react-bootstrap";

function TargetAudience() {
  return (
    <Container className="vis-page" fluid>
      <Container>
        <h2>Target Audience</h2>
        <h4>Player</h4>
        <p>
          The most common type of target audience is the player. There are no
          strictly defined limits for visualizations targeted at players, they
          can be formal, casual, or in any format developers choose.
        </p>
        <h4>Developer</h4>
        <p>
          Visualizations are a helpful tool for game developers to display game
          analytics, telemetry, and logs. Analysis of game metrics is an
          integral part of achieving balanced gameplay.
        </p>
        <h4>Observer</h4>
        <p>
          Watching other people play the game in real-time or in replays is a
          valuable asset for the gaming community. Competitive games such as
          Counter-Strike: Global Offensive or real-time strategy games such as
          StarCraft present very useful visualizations for the game's observers.
          Visualizations display various data such as resources, action per
          minute, army compositions, and technology research in case of
          strategies. For shooters, it is the player's equipment, remaining
          members of the team, or even a path of grenade throws.
        </p>
      </Container>
    </Container>
  );
}

export default TargetAudience;
