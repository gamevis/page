import { gql } from "@apollo/client";

export const GET_USER = gql`
  query GET_MY_PROFILE($id: String!) {
    user(where: { auth0_id: { _eq: $id } }) {
      auth0_id
      created_at
      email
      last_seen
      name
      role
    }
  }
`;

export const GET_VISUALIZATION = gql`
  query GET_VISUALIZATION($id: uuid!, $userId: String!) {
    visualization_by_pk(id: $id) {
      id
      created_at
      photo
      name
      release_year
      note
      source
      canva_x
      canva_y
      canva_width
      canva_height
      tag_visualizations_aggregate {
        aggregate {
          count(columns: voter_id, distinct: true)
        }
      }
      tag_visualizations(order_by: { tag: { category: { priority: asc } } }) {
        voter_id
        tag {
          category {
            id
            name
            priority
          }
          category_id
          description
          id
          name
        }
      }
    }
    category(order_by: { priority: asc }) {
      id
      name
      priority
      description
      tags(
        order_by: { tag_visualizations_aggregate: { count: desc_nulls_last } }
      ) {
        description
        id
        name
        category_id
        tag_visualizations(
          where: {
            visualization_id: { _eq: $id }
            _and: { voter_id: { _eq: $userId } }
          }
        ) {
          voter_id
          tag_id
          visualization_id
        }
        tag_visualizations_aggregate(
          where: { visualization_id: { _eq: $id } }
        ) {
          aggregate {
            count
          }
        }
      }
    }
  }
`;

export const VOTE_ADD = gql`
  mutation VOTE_ADD(
    $tag_id: Int!
    $voter_id: String!
    $visualization_id: uuid!
  ) {
    insert_tag_visualization_one(
      object: {
        tag_id: $tag_id
        voter_id: $voter_id
        visualization_id: $visualization_id
      }
    ) {
      id
    }
  }
`;

export const VOTE_DEL = gql`
  mutation VOTE_DEL(
    $tag_id: Int!
    $voter_id: String!
    $visualization_id: uuid!
  ) {
    delete_tag_visualization(
      where: {
        visualization_id: { _eq: $visualization_id }
        _and: {
          voter_id: { _eq: $voter_id }
          _and: { tag_id: { _eq: $tag_id } }
        }
      }
    ) {
      affected_rows
    }
  }
`;

export const GET_VISUALIZATIONS = gql`
  query GET_VISUALIZATIONS {
    visualization(offset: 0, order_by: { created_at: desc, name: asc }) {
      id
      created_at
      photo
      name
      release_year
      note
      source
      canva_x
      canva_y
      canva_width
      canva_height
      tag_visualizations_aggregate {
        aggregate {
          count(columns: voter_id, distinct: true)
        }
      }
      tag_visualizations(order_by: [{ tag: { category: { priority: asc } } }]) {
        id
        tag_id
        visualization_id
        voter_id
        tag {
          category {
            id
            name
            priority
            description
            tags_aggregate(order_by: { category: { priority: asc } }) {
              aggregate {
                count
              }
            }
            tags {
              tag_visualizations_aggregate {
                aggregate {
                  count
                }
              }
            }
          }
          category_id
          description
          id
          name
          tag_visualizations_aggregate {
            aggregate {
              count
            }
          }
        }
      }
    }
    category(order_by: { priority: asc }) {
      id
      name
      priority
      description
      tags(
        order_by: { tag_visualizations_aggregate: { count: desc_nulls_last } }
      ) {
        description
        id
        name
        category_id
        tag_visualizations {
          voter_id
          tag_id
          visualization_id
        }
        tag_visualizations_aggregate {
          aggregate {
            count
          }
        }
      }
    }
  }
`;
