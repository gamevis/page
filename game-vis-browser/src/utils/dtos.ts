// DTOs

// COMMON

export interface UserReply {
  user: User[];
}

export interface User {
  auth0_id: string;
  email: string;
  name: string;
  role: string;
  created_at: string;
  last_seen: string;
}

// GET SINGLE VIS

export interface VisualizationReply {
  visualization_by_pk: Visualization;
  category: Category[];
}

export interface Visualization {
  id: string;
  created_at: string;
  photo: string;
  name: string;
  release_year: number;
  tag_visualizations: Tags[];
  note: string;
  source: string;
  tag_visualizations_aggregate: TagVisAggregate;
  canva_x: number;
  canva_y: number;
  canva_width: number;
  canva_height: number;
}

export interface Tags {
  voter_id: string;
  tag: Tag;
}

export interface Tag {
  id: number;
  name: string;
  category_id: number;
  description: string;
  tag_visualizations: TagVis[];
  tag_visualizations_aggregate: TagVisAggregate;
}

export interface TagVisAggregate {
  aggregate: TagVisCount;
}

export interface TagVisCount {
  count: number;
}

export interface TagVis {
  voter_id: string;
  tag_id: number;
  visualization_id: string;
}

export interface Category {
  id: number;
  name: string;
  priority: number;
  description: string;
  tags: Tag[];
}

// GET ALL VIS

export interface AllVisualizationReply {
  visualization: AllVisualization[];
  category: Category[];
}

export interface AllVisualization {
  id: string;
  created_at: string;
  photo: string;
  name: string;
  release_year: number;
  note: string;
  source: string;
  tag_visualizations: AllTags[];
  tag_visualizations_aggregate: TagVisAggregate;
  canva_x: number;
  canva_y: number;
  canva_width: number;
  canva_height: number;
}

export interface AllTags {
  id: number;
  tag_id: number;
  visualization_id: string;
  voter_id: string;
  tag: AllTag;
}

export interface AllTag {
  id: number;
  name: string;
  category_id: number;
  category: AllCategory;
  description: string;
  tag_visualizations_aggregate: TagVisAggregate;
}

export interface AllVotesTag {
  tag_visualizations_aggregate: TagAggregate;
}

export interface AllCategory {
  id: number;
  name: string;
  priority: number;
  description: string;
  tags_aggregate: TagAggregate;
  tags: AllVotesTag[];
}

export interface TagAggregate {
  aggregate: TagCount;
}

export interface TagCount {
  count: number;
}
