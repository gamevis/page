//Public constants for backend APIs
export const authClientId = "PAGyvAaywomf6rwQ5X0qMKg2RmLurk84";
export const authDomain = "game-vis-browser.eu.auth0.com";
export const callbackUrl = "https://localhost:3000/callback";
export const hasuraUrl = "https://game-vis-browser.hasura.app/v1/graphql";
export const authAudience = "https://game-vis-browser.eu.auth0.com/api/v2/";
export const publicUrl = "https://gamevis.gitlab.io/page/";

//Constants for UI control
export const staticCategories = [1,2,15];
