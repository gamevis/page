import { Route, Routes } from "react-router-dom";
import Profile from "../auth/Profile";
import ItemPage from "../browser/ItemPage";
import VisBrowser from "../browser/VisBrowser";
import Main from "../home/Main";
import About from "../pages/About";
import Category from "../pages/Category";
import Statistics from "../pages/Statistics";
import Taxonomy from "../pages/Taxonomy";

function Routing() {
  return (
    <Routes>
      <Route path="/" element={<Main />}>
        <Route path="" element={<VisBrowser />} />
        <Route path="about" element={<About />} />
        <Route path="statistics" element={<Statistics />} />
        <Route path="taxonomy" element={<Taxonomy />} />
        <Route path="profile" element={<Profile />} />
        <Route path="vis">
          <Route path=":id" element={<ItemPage />} />
        </Route>
        <Route path="category">
          <Route path=":id" element={<Category />} />
        </Route>
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Route>
    </Routes>
  );
}

export default Routing;
