import { Container, Nav, Navbar } from "react-bootstrap";
import NavbarCollapse from "react-bootstrap/esm/NavbarCollapse";
import { Link, useLocation } from "react-router-dom";
import { useRecoilState } from "recoil";
import AuthButton from "../auth/AuthButton";
import { userAuthState } from "../auth/userState";

function Topbar() {
  const [appUserAuth, setAppUserAuthState] = useRecoilState(userAuthState);
  const urlPath = useLocation().pathname;

  return (
    <div className="navbar" style={{ zIndex: "2" }}>
      <Navbar
        collapseOnSelect
        bg="dark"
        variant="dark"
        sticky="top"
        expand="md"
      >
        <Container fluid>
          <Link className="link" to="/">
            <Navbar.Brand as="div">
              <img
                alt="Game Visualization Browser Logo"
                src="https://game-vis-browser.s3.eu-central-1.amazonaws.com/game_vis_logo.png"
                width="30"
                height="30"
                className="d-inline-block align-top logo"
              />
              <span>Game Vis Browser</span>
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <NavbarCollapse id="responsive-navbar-nav">
            <Nav activeKey={urlPath}>
              <Nav.Item>
                <Nav.Link as="div" eventKey="/">
                  <Link className="nav-link" to="/">
                    Browser
                  </Link>
                </Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link as="div" eventKey="/statistics">
                  <Link className="nav-link" to="/statistics">
                    Statistics
                  </Link>
                </Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link as="div" eventKey="/taxonomy">
                  <Link className="nav-link" to="/taxonomy">
                    Taxonomy
                  </Link>
                </Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link as="div" eventKey="/about">
                  <Link className="nav-link" to="/about">
                    About
                  </Link>
                </Nav.Link>
              </Nav.Item>

              {appUserAuth && (
                <Nav.Item>
                  <Nav.Link as="div" eventKey="/profile">
                    <Link className="nav-link" to="/profile">
                      Account
                    </Link>
                  </Nav.Link>
                </Nav.Item>
              )}

              <Nav.Item>
                <Nav.Link as="div" eventKey="/login">
                  <AuthButton />
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </NavbarCollapse>
        </Container>
      </Navbar>
    </div>
  );
}

export default Topbar;
