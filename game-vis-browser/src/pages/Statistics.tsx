import { useQuery } from "@apollo/client";
import { useEffect } from "react";
import { Container, Spinner } from "react-bootstrap";
import { useRecoilState } from "recoil";
import { categoriesState, visualizationsState } from "../auth/userState";
import StatisticsTable from "../components/StatisticsTable";
import {
  AllVisualization,
  AllVisualizationReply,
  Category,
} from "../utils/dtos";
import { GET_VISUALIZATIONS } from "../utils/queries";

function Statistics() {
  const { loading, error, data } =
    useQuery<AllVisualizationReply>(GET_VISUALIZATIONS);
  const [vis, setVis] = useRecoilState<AllVisualization[]>(visualizationsState);
  const [categories, setCategories] =
    useRecoilState<Category[]>(categoriesState);

  useEffect(() => {
    if (data) {
      setVis(data.visualization);
      setCategories(data.category);
    }
  }, [data]);

  return (
    <Container className="about">
      <h2>Game Visualitions Browser - Usage Statistics</h2>

      <div>
        {loading && vis.length === 0 && (
          <Spinner
            as="span"
            animation="border"
            role="status"
            aria-hidden="true"
          />
        )}
        {error && <>There was a problem when reaching backend</>}
        {!error && vis.length > 0 && (
          <StatisticsTable vis={vis} categories={categories} />
        )}
      </div>
    </Container>
  );
}

export default Statistics;
