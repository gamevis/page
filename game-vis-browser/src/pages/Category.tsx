import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ReactNode } from "react";
import { Button, Container } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { useRecoilState } from "recoil";
import { returnUrlState } from "../auth/userState";
import AnalyticTask from "../category/AnalyticTask";
import Immersion from "../category/Immersion";
import PatternGroup from "../category/PatternGroup";
import PrimaryPurpose from "../category/PrimaryPurpose";
import Spatiality from "../category/Spatiality";
import TargetAudience from "../category/TargetAudience";
import TemportalUsage from "../category/TemporalUsage";
import VisualComplexity from "../category/VisualComplexity";
import Visualization from "../category/Visualization";
import VisualizationTask from "../category/VisualizationTask";

type ItemDetailProps = {
  id: string;
};

function Category() {
  const navigate = useNavigate();
  const [returnUrlValue, setReturnUrl] = useRecoilState(returnUrlState);
  const { id } = useParams<ItemDetailProps>();

  const categories = new Map<number, ReactNode>([
    [3, <Spatiality />],
    [4, <Immersion />],
    [5, <Visualization />],
    [6, <Visualization />],
    [7, <Visualization />],
    [8, <VisualizationTask />],
    [9, <PatternGroup />],
    [10, <PrimaryPurpose />],
    [11, <TargetAudience />],
    [12, <TemportalUsage />],
    [13, <VisualComplexity />],
    [14, <AnalyticTask />],
  ]);

  return (
    <>
      <Container className="vis-page" fluid>
        <Button
          id="navigate-back"
          onClick={() => {
            navigate(returnUrlValue);
          }}
          variant="outline-dark"
        >
          <h1 className="mb-0">
            <FontAwesomeIcon icon={faArrowLeft} />{" "}
          </h1>
        </Button>

        <Container>
          {!categories.get(id ? parseInt(id) : 0) && <div> Not found</div>}
          {categories.get(id ? parseInt(id) : 0)}
        </Container>
      </Container>
    </>
  );
}

export default Category;
