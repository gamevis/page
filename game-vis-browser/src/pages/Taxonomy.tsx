import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Container, Nav } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { useRecoilState } from "recoil";
import { returnUrlState } from "../auth/userState";

function Taxonomy() {
  const [returnUrlValue, setReturnUrl] = useRecoilState(returnUrlState);
  const navigate = useNavigate();
  return (
    <>
      <Container className="vis-page" fluid>
        <Button
          id="navigate-back"
          onClick={() => {
            navigate(returnUrlValue);
          }}
          variant="outline-dark"
        >
          <h1 className="mb-0">
            <FontAwesomeIcon icon={faArrowLeft} />{" "}
          </h1>
        </Button>
        <Container>
          <Container className="vis-page" fluid>
            <h2>Taxonomy</h2>
            <p>
              Taxonomy sourced from Master's thesis Game Visualization Browser{" "}
              <a href="https://is.muni.cz/th/ffu6r/" target="_blank">
                Link
              </a>
            </p>
            <Nav className="flex-column">
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/10"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Primary Purpose
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/11"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Target Audience
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/12"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Temportal Usage
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/13"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Visual Complexity
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/4"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Immersion
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/8"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Visualization Task
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/9"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Pattern Group
                </Link>
              </Nav.Link>

              <Nav.Link>
                <Link
                  className="link"
                  to="/category/3"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Spatiality
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/14"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Analytic Task
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link
                  className="link"
                  to="/category/5"
                  onClick={() => setReturnUrl("/taxonomy")}
                >
                  Common Visualization classification
                </Link>
              </Nav.Link>
            </Nav>
          </Container>
        </Container>
      </Container>
    </>
  );
}

export default Taxonomy;
