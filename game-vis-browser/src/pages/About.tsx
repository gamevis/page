import { Container } from "react-bootstrap";

function About() {
  return (
    <Container className="about">
      <h2>Game Visualitions Browser</h2>
      <p>
        This page was created as part of master's thesis that aim for
        establishing taxonomy of visualizations used in games. On this page, you
        can browse categorized Visualizations used in games. Categories are
        votable and this can be viewed as public opinion to where the game
        visualizations fit the best.
      </p>
      <h4>Survey of visualizations used in games</h4>
      <p>
        This website wants to collect public opinion about the categorization of
        visualization used in games. Anyone who creates an account can submit an
        opinion about the correct categorization of visualizations.
        <br />
        The results of these opinions can then be used as the basis for future
        research into this topic and future observations. The main goal is to
        collect more information over time.
      </p>
      <h4>Voting process</h4>
      <p>
        Selection of invidual tags in each category, that are taken as
        acceptable are displayed in popup detail of invididual visualizations.
        These tags are further used as basis for filtering. <br /> No design
        space/classification is ever truly complete and perfect. However, there
        can be bias when considering which category visualization should be
        assigned, and to refine the precision, we opted to collect feedback from
        the public when classifying. Such feedback will be collected in the form
        of votes.
        <br />
        Visualization tags that are categories of games such as game genre etc.
        are not votable.
        <br />
        <br />
        <b>Formula for displaying voted tags:</b> <br />
        Votes for selected tag <b>{">="}</b> Average vote count of selected
        category
      </p>
      <hr />
      <h4>Master's thesis</h4>
      <p>
        <i>Official name: </i>
        <br />
        <strong>Game Visualization Browser</strong>
        <br />
        <i>Supervisor:</i>
        <br />
        <span>Priv.-Doz. Dipl.-Ing. Dr. Simone Kriglstein, učo 112812</span>
        <br />
        <i>Co-Supervisor:</i>
        <br />
        <span>
          Prof. Günter Wallner, profile{" "}
          <a
            style={{ color: "black" }}
            href="https://www.jku.at/en/institute-of-computer-graphics/about-us/team/guenter-wallner/"
            className=""
          >
            here
          </a>
        </span>
        <br />
        <i>Author: </i>
        <br />
        <span>Bc. Márius Molčány, učo 456350</span>
        <br />
        <br />
        <i>Description: </i>
        <br />
        The goal of this master thesis is to analyze the Game UI database
        (https://gameuidatabase.com/) regarding visualizations which are used as
        part of game interfaces. The different games and screenshots in the
        database should be categorized according to the used visualizations. The
        visualizations should be analyzed with respect to different criteria
        such as genre, tasks, year, and others. Based on the findings and
        categorization a visualization browser (similar to
        https://textvis.lnu.se/) that offers an overview about the used
        visualizations in games and allows to filter based on different aspects
        should be developed.
      </p>

      <i>Full text of thesis: </i>
      <br />
      <a
        style={{ color: "black" }}
        href="https://is.muni.cz/th/ffu6r/"
        target="_blank"
      >
        Game Visualization Browser
      </a>
      <hr />
      <h4>Technology stack</h4>
      <p>Build as React Single page application hosted on GitLab pages</p>
      <p>Authentication provided by Auth0</p>
      <p>Backend created in Hasura hosted in Hasura Cloud</p>
      <p>PostgreSQL database hosted in Heroku Cloud </p>
      <p>Authentication provided by Auth0</p>
      <p>Images used on this page hosted in Amazon S3 - Cloud Object Storage</p>
      <p>
        Performance limitations: <br />
        Due to using free tier services from all of the above-mentioned
        solutions, the page may experience temporary outages due to excessive
        load. <br />
        As example of the limits, Hasura backend allows 60 requests per minute,
        so if the page is overloaded by requests, it will not be available
        approximatly for 1 minute, until the limit resets.
      </p>
      <hr />
      <h4>Author</h4>
      <p>Created by Márius Molčány </p>
      <p>Contact: {"456350@mail.muni.cz"}</p>
    </Container>
  );
}

export default About;
