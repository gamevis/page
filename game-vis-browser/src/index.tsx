import { Auth0Provider } from "@auth0/auth0-react";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { RecoilRoot } from "recoil";
import App from "./App";
import AuthApolloProvider from "./auth/AuthApolloProvider";
import reportWebVitals from "./reportWebVitals";
import "./style.css";
import { authAudience, authClientId, authDomain, publicUrl } from "./utils/constants";

ReactDOM.render(
  <React.StrictMode>
    <Auth0Provider
      domain={authDomain}
      clientId={authClientId}
      redirectUri={
        process.env.NODE_ENV == "development"
          ? window.location.origin
          : publicUrl
      }
      audience={authAudience}
      scope="read:current_user update:current_user_metadata"
    >
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <RecoilRoot>
          <AuthApolloProvider>
            <App />
          </AuthApolloProvider>
        </RecoilRoot>
      </BrowserRouter>
    </Auth0Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
