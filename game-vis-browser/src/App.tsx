import Routing from "./common/Routing";
import Topbar from "./common/Topbar";

function App() {
  return (
    <div className="App">
      <Topbar />
      <Routing />
    </div>
  );
}

export default App;
