import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import {
  Accordion,
  Button,
  ButtonGroup,
  Form,
  Placeholder,
  ToggleButton,
} from "react-bootstrap";
import { MultiSelect } from "react-multi-select-component";
import { useRecoilState } from "recoil";
import {
  browserState,
  orderAscState,
  orderState,
  searchOrSwitch,
  searchTagsState,
  searchTextState,
  sidebarState,
} from "../browser/filterState";
import FilterSelect from "../components/FilterSelect";
import { staticCategories } from "../utils/constants";
import { Category } from "../utils/dtos";

type SidebarProps = {
  categories: Category[];
  loading: boolean;
};

function Sidebar(props: SidebarProps) {
  const [width, setWidth] = useState(window.innerWidth);
  const [searchTextVal, setSearchText] = useRecoilState(searchTextState);
  const [searchTags, setSearchTags] = useRecoilState(searchTagsState);
  const [orSwitch, setOrSwitch] = useRecoilState(searchOrSwitch);
  const [order, setOrder] = useRecoilState(orderState);
  const [orderAsc, setOrderAsc] = useRecoilState(orderAscState);
  const [checked, setChecked] = useState(false);
  const [sidebarShow, setSidebarShow] = useRecoilState(sidebarState);
  const [browserShow, setBrowserShow] = useRecoilState(browserState);

  function resetSearch() {
    setSearchText("");
    setSearchTags([]);
    setOrSwitch(false);
  }

  useEffect(() => {
    function handleResize() {
      setWidth(window.innerWidth);
    }
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [width]);

  useEffect(() => {
    if (width > 600) {
      setSidebarShow(true);
      setBrowserShow(true);
    }
  }, [width]);

  return (
    <>
      {sidebarShow && (
        <div className="Sidebar">
          <Button
            id="sidebar-hide"
            className="mb-3"
            variant="outline-primary"
            onClick={() => {
              setSidebarShow(false);
              setBrowserShow(true);
            }}
          >
            <FontAwesomeIcon icon={faArrowLeft} /> Hide filters
          </Button>
          <Form className="Search">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                placeholder="Search..."
                onChange={(e) => setSearchText(e.target.value)}
                value={searchTextVal}
              />
            </Form.Group>
          </Form>

          <Accordion defaultActiveKey={["1"]} alwaysOpen>
            <Accordion.Item eventKey="0">
              <Accordion.Header>Order by</Accordion.Header>
              <Accordion.Body>
                <ButtonGroup className="mb-2">
                  <ToggleButton
                    className="p-2"
                    id="toggle-order-0"
                    type="checkbox"
                    variant="outline-primary"
                    checked={order === 0}
                    value={0}
                    onChange={() => setOrder(0)}
                  >
                    Name
                  </ToggleButton>
                  <ToggleButton
                    className="p-2"
                    id="toggle-order-1"
                    type="checkbox"
                    variant="outline-primary"
                    checked={order === 1}
                    value={1}
                    onChange={() => setOrder(1)}
                  >
                    Added
                  </ToggleButton>
                  <ToggleButton
                    className="p-2"
                    id="toggle-order-2"
                    type="checkbox"
                    variant="outline-primary"
                    checked={order === 2}
                    value={2}
                    onChange={(e) => setOrder(2)}
                  >
                    Votes
                  </ToggleButton>

                  <ToggleButton
                    className="p-2"
                    id="toggle-order-3"
                    type="checkbox"
                    variant="outline-primary"
                    checked={order === 3}
                    value={3}
                    onChange={(e) => setOrder(3)}
                  >
                    Year
                  </ToggleButton>
                </ButtonGroup>
                <br />
                <ButtonGroup className="mx-auto">
                  <ToggleButton
                    className="p-2"
                    id="toggle-order-asc"
                    type="checkbox"
                    variant="outline-primary"
                    checked={orderAsc === 1}
                    value={0}
                    onChange={() => setOrderAsc(1)}
                  >
                    Ascending &#8593;
                  </ToggleButton>
                  <ToggleButton
                    className="p-2"
                    id="toggle-order-desc"
                    type="checkbox"
                    variant="outline-primary"
                    checked={orderAsc === -1}
                    value={-1}
                    onChange={() => setOrderAsc(-1)}
                  >
                    Descending &#8595;
                  </ToggleButton>
                </ButtonGroup>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1">
              <Accordion.Header>Visualitions filters</Accordion.Header>
              <Accordion.Body>
                {props.loading &&
                  [...Array(12)].map((x, index) => {
                    return (
                      <div className="mb-2" key={index}>
                        <Placeholder as="b" animation="wave">
                          <Placeholder xs={5} />
                        </Placeholder>
                        <MultiSelect
                          isLoading={true}
                          hasSelectAll={false}
                          disableSearch={true}
                          options={[]}
                          value={[]}
                          labelledBy={"X"}
                        />
                      </div>
                    );
                  })}
                {!props.loading &&
                  props.categories?.map((category) => {
                    return (
                      !staticCategories.includes(category.id) && (
                        <FilterSelect key={category.id} category={category} />
                      )
                    );
                  })}
                {false && !props.loading && (
                  <ButtonGroup>
                    <ToggleButton
                      id={`radio-1`}
                      type="radio"
                      variant={"outline-primary"}
                      name="radio"
                      value={1}
                      checked={!orSwitch}
                      onChange={() => setOrSwitch(false)}
                    >
                      AND &and;
                    </ToggleButton>
                    <ToggleButton
                      id={`radio-2`}
                      type="radio"
                      variant={"outline-primary"}
                      name="radio"
                      value={0}
                      checked={orSwitch}
                      onChange={() => setOrSwitch(true)}
                    >
                      OR &or;
                    </ToggleButton>
                  </ButtonGroup>
                )}
                {false && !props.loading && (
                  <Button
                    className="mx-3"
                    variant="primary"
                    onClick={resetSearch}
                  >
                    RESET
                  </Button>
                )}
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="2">
              <Accordion.Header>Game specific filters</Accordion.Header>
              <Accordion.Body>
                {props.loading &&
                  [...Array(3)].map((x, index) => {
                    return (
                      <div className="mb-2" key={index}>
                        <Placeholder as="b" animation="wave">
                          <Placeholder xs={5} />
                        </Placeholder>
                        <MultiSelect
                          isLoading={true}
                          hasSelectAll={false}
                          disableSearch={true}
                          options={[]}
                          value={[]}
                          labelledBy={"X"}
                        />
                      </div>
                    );
                  })}
                {!props.loading &&
                  props.categories?.map((category) => {
                    return (
                      staticCategories.includes(category.id) && (
                        <FilterSelect key={category.id} category={category} />
                      )
                    );
                  })}
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
      )}
    </>
  );
}

export default Sidebar;
